DECLARE @Sql NVARCHAR(500) DECLARE @Cursor CURSOR

SET @Cursor = CURSOR FAST_FORWARD FOR
SELECT DISTINCT sql = 'ALTER TABLE [' + tc2.TABLE_NAME + '] DROP [' + rc1.CONSTRAINT_NAME + ']'
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc1
LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc2 ON tc2.CONSTRAINT_NAME =rc1.CONSTRAINT_NAME

OPEN @Cursor FETCH NEXT FROM @Cursor INTO @Sql

WHILE (@@FETCH_STATUS = 0)
BEGIN
Exec sp_executesql @Sql
FETCH NEXT FROM @Cursor INTO @Sql
END

CLOSE @Cursor DEALLOCATE @Cursor
GO

EXEC sp_MSforeachtable 'DROP TABLE ?'
GO

CREATE TABLE Lista
(
   Name      [NVARCHAR](50) not null  PRIMARY KEY, -- primary key column
);


CREATE TABLE Contacto
(
   Email        [NVARCHAR](50)    NOT NULL   PRIMARY KEY, -- primary key column
   Data_criacao datetime DEFAULT(getdate())
);

CREATE TABLE Contacto_Lista
(
   Email        [NVARCHAR](50)    NOT NULL, -- primary key column
   lista [NVARCHAR](50) not null,

   PRIMARY KEY (email,lista),

   CONSTRAINT fk FOREIGN KEY (lista)     
    REFERENCES Lista (Name),
   CONSTRAINT contacto_fk FOREIGN KEY (Email)     
    REFERENCES Contacto (Email)      
);


CREATE TABLE Parametros
(
   Num_Parametro int     NOT NULL,
   email    [NVARCHAR](50) not null,
   lista [NVARCHAR](50),
   parametro [NVARCHAR](50) not null,
   Primary key (email,lista,parametro),  
   CONSTRAINT email_parametros_fk FOREIGN KEY (email)     
    REFERENCES Contacto (Email),
    CONSTRAINT lista_parametros_fk FOREIGN KEY (lista)     
    REFERENCES Lista (Name)     
);

CREATE TABLE Campanha
(
   nome    [NVARCHAR](50) not null primary key,
   data_criacao datetime DEFAULT(getdate()),
   data_envio datetime ,
   lista [NVARCHAR](50) not null,
   template [NVARCHAR](1000) not null,
   enviado tinyint not null,

   CONSTRAINT lista_campanha_fk FOREIGN KEY (lista)     
    REFERENCES Lista (Name)        
);


CREATE TABLE Envios
(
   id int  not null IDENTITY(1,1) primary key,
   data_added datetime not null DEFAULT(getdate()),
   data_sent datetime ,
   data_failed datetime ,
   data_read datetime ,
   campanha [NVARCHAR](50) not null,
   email_body [NVARCHAR](2000) not null,
   email [NVARCHAR](50) not null,
   estado [NVARCHAR](10) not null,

   CONSTRAINT campanha_fk FOREIGN KEY (campanha)     
    REFERENCES Campanha (nome),
    CONSTRAINT email_envio_fk FOREIGN KEY (email)     
    REFERENCES Contacto (email)     
);


-- solucao da primeira query 
select r.template,r.Month_Year,r.Reads from ( 
   -- query com o template, numero de envios e mes/ano agrupados por mês e por template
   select c.template as template,count(c.template) as Reads, DATEADD(MONTH, DATEDIFF(MONTH, 0, e.data_sent), 0) as Month_Year from campanha as c,envios as e
         where c.nome=e.campanha and e.estado = 'read' group by DATEADD(MONTH, DATEDIFF(MONTH, 0, e.data_sent), 0),c.template)
   as r 
   where r.Reads = (select max(r2.Reads) from ( 
      -- query que retorna para cada iteração da query anterior o valor máximo de envios no dado mês
      select c.template as template,count(c.template) as Reads from campanha as c,envios as e
         where c.nome=e.campanha and e.estado = 'read' and DATEADD(MONTH, DATEDIFF(MONTH, 0, e.data_sent), 0) = r.Month_Year
           group by DATEADD(MONTH, DATEDIFF(MONTH, 0, e.data_sent), 0),c.template)
       as r2) 


-- solucao da segunda query 

-- selecionado o primeiro da lista
select TOP 1 enviados.campanha as Campanha,lidos.num as Lidos,enviados.num as Enviados,
cast(round((CONVERT(decimal(3,1), lidos.num)/enviados.num),2) as numeric(36,2))
 as Rácio from (
         -- todos os envios com excecao dos 'added'
         select e.campanha, count(e.id) as num from envios as e, campanha as c
         where e.campanha=c.nome and (not e.estado='added') group by e.campanha ) as enviados,
         -- os envios 'read'
         (select e.campanha, count(e.id) as num from envios as e, campanha as c
         where e.campanha=c.nome and e.estado='read' group by e.campanha) as lidos
         -- ordenados de forma descendente
where enviados.campanha = lidos.campanha order by Rácio DESC 

--solucao da terceira query
select distinct c.email from contacto as c, lista as l, Contacto_Lista as cl, Parametros as p
where cl.Email=c.Email and cl.lista=l.Name and p.email=cl.Email and p.lista=cl.lista and p.parametro='Bob'


