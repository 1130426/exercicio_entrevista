
# coding=utf-8

import csv
import json
import pyodbc
import sys  

reload(sys)  
sys.setdefaultencoding('utf8')


campaigns_path = 'dados/campaigns.csv'

contacts_path = 'dados/contactslists.json'

envios_path = 'dados/sends.csv'

listas_contactos = []

# carregar lista de contactos
with open(contacts_path) as c:
    data = json.load(c)
    for row in data['contactlists']:
        listas_contactos.append(row)


# criar connection
connection = pyodbc.connect(
    'DRIVER={ODBC Driver 17 for SQL Server};SERVER=127.0.0.1,1433;UID=SA;PWD=Admin1234')
cursor = connection.cursor()

# criar listas de contactos

for name in listas_contactos:
    cursor.execute("insert into lista(name) values (?)", name)
    connection.commit()


#inserir contactos e respetivos parametros

for lista in data['contactlists']:
    # print("           ")
    # print(lista)
    # print("----------------------- ")
    for contactos in data['contactlists'][lista]:
        email = contactos['email']
        params = contactos['params']

        exists = cursor.execute(
            "select email from contacto where email = ?", email).fetchval()
        if exists:
            print('o email', email,
                  'ja existe e foram lhe adicionadas novas informacoes.')
            exists = None
        else:
            print('o email', email, 'foi criado com sucesso!')
            cursor.execute("insert into contacto(email) values (?)", email)
            connection.commit()

        cursor.execute(
            "insert into Contacto_Lista(email,lista) values (?,?)", email, lista)
        connection.commit()
        count=1
        for p in params:
            cursor.execute(
                "insert into parametros(email,lista,parametro,num_parametro) values (?,?,?,?)", email, lista,p,count)
            connection.commit()
            count+=1

# carregar lista de campanhas
with open(campaigns_path, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    line_count = 0
    for row in reader:
        if line_count == 0:
            print(row[0], row[1], row[2], row[3])  
            line_count += 1
        else:
            print(row[0], row[1], row[2], row[3])
            if  row[3]=='yes':
                sent=1
                print(sent)
            else:
                sent=0
                print(sent)
            print("INSERTS ------------")
            cursor.execute(
                "insert into campanha(nome,template,lista,enviado) values (?,?,?,?)", row[0], row[1],row[2],sent)
            connection.commit()
            line_count += 1


# carregar lista de envios
with open(envios_path, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    line_count = 0
    for row in reader:
        if line_count == 0:
            line_count += 1
        else:
            campanha = row[0]
            email= row[1]
            #email body vai ser construido
            estado = row[3]
            data_added = row[4]
            data_sent = row[5]
            data_failed = row[6]
            data_read = row[7]

            # get template
            template = cursor.execute(
             "select campanha.template from campanha where campanha.nome = ?"
             ,campanha).fetchval()
            if template:
                print("-----------------",email,"------------------")
                cursor.execute(
                "select parametros.parametro,parametros.num_parametro from parametros,campanha where parametros.email = ? and campanha.nome = ? and campanha.lista=parametros.lista"
                , email,campanha)
                email_body ='{}'.format(template) 
                # preencher template com parametros da respetiva lista
                for p in cursor.fetchall():
                    num=p.num_parametro
                    antigo='{{param{}}}'.format(num)
                    substituto='{}'.format(p.parametro)
                    print('Antigo = ',antigo,'  Novo = ',substituto)
                    email_body=email_body.replace(antigo,substituto)

                print(email_body)

                cursor.execute(
                    "insert into envios(campanha,email,email_body,estado,data_added,data_sent,data_failed,data_read) values (?,?,?,?,?,?,?,?)"
                    , campanha,email,email_body,estado,data_added,data_sent,data_failed,data_read)
                connection.commit()
            else:
                print("Campanha especificada não existe ou não tem template.")
            line_count += 1
        

